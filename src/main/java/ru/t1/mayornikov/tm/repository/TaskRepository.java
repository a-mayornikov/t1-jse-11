package ru.t1.mayornikov.tm.repository;

import ru.t1.mayornikov.tm.api.repository.ITaskRepository;
import ru.t1.mayornikov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public Task create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public Task create(final String name) {
        final Task task = new Task();
        task.setName(name);
        return add(task);
    }

    @Override
    public Task findOne(final String id) {
        for (final Task task: tasks) if (id.equals(task.getId())) return task;
        return null;
    }

    @Override
    public Task findOne(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task remove(final Task task) {
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task remove(final String id) {
        final Task task = findOne(id);
        if (task == null) return null;
        return remove(task);
    }

    @Override
    public Task remove(final Integer index) {
        final Task task = findOne(index);
        if (task == null) return null;
        return remove(task);
    }

}