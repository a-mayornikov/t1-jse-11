package ru.t1.mayornikov.tm.service;

import ru.t1.mayornikov.tm.api.repository.IProjectRepository;
import ru.t1.mayornikov.tm.api.service.IProjectService;
import ru.t1.mayornikov.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository IProjectRepository) {
        this.projectRepository = IProjectRepository;
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    @Override
    public Project findOne(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findOne(id);
    }

    @Override
    public Project findOne(final Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.findOne(index);
    }

    @Override
    public Project remove(final Project project) {
        if (project == null) return null;
        return projectRepository.remove(project);
    }

    @Override
    public Project remove(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.remove(id);
    }

    @Override
    public Project remove(final Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.remove(index);
    }

    @Override
    public Project add(final Project project) {
        if(project == null) return null;
        return projectRepository.add(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project update(String id, String name, String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findOne(id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project update(Integer index, String name, String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findOne(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}