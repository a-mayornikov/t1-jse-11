package ru.t1.mayornikov.tm.api.service;

import ru.t1.mayornikov.tm.api.repository.IProjectRepository;
import ru.t1.mayornikov.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project update(String id, String name, String description);

    Project update(Integer index, String name, String description);

}
