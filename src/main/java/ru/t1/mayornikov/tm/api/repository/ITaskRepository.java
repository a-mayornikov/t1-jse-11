package ru.t1.mayornikov.tm.api.repository;

import ru.t1.mayornikov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clear();

    List<Task> findAll();

    Task create(String name, String description);

    Task create(String name);

    Task findOne(String id);

    Task findOne(Integer index);

    Task remove(Task task);

    Task remove(String id);

    Task remove(Integer index);

}